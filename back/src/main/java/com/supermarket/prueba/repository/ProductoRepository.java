package com.supermarket.prueba.repository;

import com.supermarket.prueba.models.Producto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductoRepository extends JpaRepository<Producto, Long> {

    @Query("SELECT p FROM Producto p WHERE p.user.username = ?1")
    List<Producto> findByUsuario(String usuario);

}
