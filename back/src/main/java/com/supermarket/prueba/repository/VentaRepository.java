package com.supermarket.prueba.repository;

import com.supermarket.prueba.models.Venta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VentaRepository extends JpaRepository<Venta, Long> {

}
