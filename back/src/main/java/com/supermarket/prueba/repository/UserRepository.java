package com.supermarket.prueba.repository;

import com.supermarket.prueba.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
    void deleteByUsername(String username);

    Optional<User> findByUsernameAndPassword(String username, String password);
}
