package com.supermarket.prueba.services;

import com.supermarket.prueba.models.User;
import com.supermarket.prueba.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserService {

    @Autowired
    private UserRepository userRepository;


    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public User getUser(String username) {
        return userRepository.findByUsername(username).orElse(null);
    }

    public User authenticate(String username, String password) {
        return userRepository.findByUsernameAndPassword(username, password).orElse(null);
    }
    public User createUser(User user) {
        if (getUser(user.getUsername()) != null) {
            throw new IllegalArgumentException("User already exists");
        }
        if (user.getUsername() == null || user.getPassword() == null) {
            throw new IllegalArgumentException("Credentials are required");
        }
        return userRepository.save(user);
    }
    public void deleteUser(String username) {
        if (getUser(username) == null) {
            throw new IllegalArgumentException("User does not exist");
        }
        userRepository.deleteByUsername(username);
    }


}

