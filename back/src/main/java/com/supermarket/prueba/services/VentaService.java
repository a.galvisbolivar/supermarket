package com.supermarket.prueba.services;

import com.supermarket.prueba.models.Producto;
import com.supermarket.prueba.models.Venta;
import com.supermarket.prueba.repository.VentaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class VentaService {

    @Autowired
    private VentaRepository ventaRepository;

    @Autowired
    private ProductoService productoService;
    public List<Venta> getAllVentas() {
        return ventaRepository.findAll();
    }

    public Venta getVenta(Long id) {
        return ventaRepository.findById(id).orElse(null);
    }

    public Venta createVenta(Venta venta) {
        Producto p = productoService.getProducto(venta.getIdProducto());
        p.setCantidad(p.getCantidad() - venta.getCantidad());
        productoService.updateProducto(p.getIdProducto(), p);
        return ventaRepository.save(venta);
    }

    public void deleteVenta(Long id) {
        ventaRepository.deleteById(id);
    }


    @Transactional
    public void updateVenta(Long id, Venta venta) {
        Venta ventaActual = ventaRepository.findById(id).orElse(null);
        int cantidadDif = 0;
        if (ventaActual == null) {
            throw new IllegalArgumentException("Venta does not exist");
        }
        Producto p = productoService.getProducto(ventaActual.getIdProducto());
        int cantidad = 0;
        if(venta.getIdProducto() == ventaActual.getIdProducto()) {
            if (venta.getCantidad() > ventaActual.getCantidad()) {
                cantidadDif = venta.getCantidad() - ventaActual.getCantidad();
                cantidad = p.getCantidad() - cantidadDif;
            } else if (venta.getCantidad() < ventaActual.getCantidad()) {
                cantidadDif = ventaActual.getCantidad() - venta.getCantidad();
                cantidad = p.getCantidad() + cantidadDif;
            }
        }if(cantidad < 0) {
            throw new IllegalArgumentException("No hay suficientes productos");
        }else {
            p.setCantidad(cantidad);
            productoService.updateProducto(p.getIdProducto(), p);
        }
        ventaActual.setCantidad(venta.getCantidad());
        ventaActual.setFecha(venta.getFecha());
        ventaActual.setIdProducto(venta.getIdProducto());
    }
}
