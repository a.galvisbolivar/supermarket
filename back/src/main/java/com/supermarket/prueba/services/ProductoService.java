package com.supermarket.prueba.services;

import com.supermarket.prueba.models.Producto;
import com.supermarket.prueba.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ProductoService {

    @Autowired
    private ProductoRepository productoRepository;

    public List<Producto> getAllProductos() {
        return productoRepository.findAll();
    }
    public Producto getProducto(Long id) {
        return productoRepository.findById(id).orElse(null);
    }
    public Producto createProducto(Producto producto) {
        return productoRepository.save(producto);
    }
    public void deleteProducto(Long id) {
        productoRepository.deleteById(id);
    }

    public void updateProducto(Long id, Producto producto) {
        Producto productoActual = productoRepository.findById(id).orElse(null);
        if (productoActual == null) {
            throw new IllegalArgumentException("Producto does not exist");
        }
        productoActual.setNombre(producto.getNombre());
        productoActual.setCantidad(producto.getCantidad());
        productoActual.setValorUnitario(producto.getValorUnitario());
        productoActual.setIdUsuario(productoActual.getIdUsuario());
        productoActual.setIdUsuarioActualizacion(producto.getIdUsuarioActualizacion());
    }

    public List<Producto> getProductosByUsuario(String usuario) {
        return productoRepository.findByUsuario(usuario);
    }
}
