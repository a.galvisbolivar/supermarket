package com.supermarket.prueba.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "venta")
public class Venta {
    @Id
    @SequenceGenerator(
            name = "id_venta_seq",
            sequenceName = "id_venta_seq",
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_venta_seq")
    private Long idVenta;

    @Column(name = "id_producto")
    private Long idProducto;

    @Column(name = "cantidad")
    private int cantidad;

    @Column(name = "fecha")
    private Date fecha;

    @OneToOne
    @JoinColumn(name = "id_producto", insertable = false, updatable = false)
    private Producto idProducto1;

    public Venta() {

    }
    public Venta( Long idProducto, int cantidad, Date fecha) {
        this.idProducto = idProducto;
        this.cantidad = cantidad;
        this.fecha = fecha;
    }

    public Long getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(Long idVenta) {
        this.idVenta = idVenta;
    }

    public Long getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Long idProducto) {
        this.idProducto = idProducto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Producto getIdProducto1() {
        return idProducto1;
    }

    public void setIdProducto1(Producto idProducto1) {
        this.idProducto1 = idProducto1;
    }


}
