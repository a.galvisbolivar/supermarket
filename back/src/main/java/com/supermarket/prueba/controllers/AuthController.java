
package com.supermarket.prueba.controllers;

import com.supermarket.prueba.models.User;
import com.supermarket.prueba.services.UserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    private UserService userService;

    @Value("${jwt.secret}")
    private String secret;

    @PostMapping("/login")
    public Object login(@RequestBody User user) {
        User userDB = userService.getUser(user.getUsername());
        if (userDB == null || !userDB.getPassword().equals(user.getPassword())) {
            return null;
        }
        String token = Jwts.builder()
                .setSubject(user.getUsername())
                .claim("authorities", AuthorityUtils.authorityListToSet(userDB.getAuthorities()))
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
        return userDB.getUsername() + ":" + token;
    }

}
