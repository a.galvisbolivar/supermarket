export function useAuthentication() {
    return sessionStorage.getItem("token");
}
export function useUser() {
    return sessionStorage.getItem("user");
}
