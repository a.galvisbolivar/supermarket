import * as React from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: "#1976d2",
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));
export default function CustomizedTables(props) {
  const header = props.header;
  const rows = props?.rows;
  const columns = rows.length > 0 ? Object.keys(rows[0]) : [];
  //keys of object to array string
  const columnsNames = columns?.map((column) => {
    return column;
  }
  );

  return (
    <TableContainer component={Paper} style={{maxWidth: 'sm'}}>
      <Table sx={{ minWidth: 700 }} aria-label="customized table">
        <TableHead>
          <TableRow>
            {header.map((headCell) => (
              <StyledTableCell key={headCell}>{headCell}</StyledTableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row, index) => (
            <StyledTableRow key={index}>
              {columnsNames.map((col, index) => (
                <StyledTableCell key={index}>{row[col]}</StyledTableCell>
              ))}
            </StyledTableRow>
          ))}
            
        </TableBody>
      </Table>
    </TableContainer>
  );
}