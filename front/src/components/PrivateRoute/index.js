
import React from "react";
import { useAuthentication } from "../../hooks";
import { Navigate, Outlet } from 'react-router-dom';

export const PrivateRoute = () => {
    const auth = useAuthentication();
    return auth ? <Outlet /> : <Navigate to="/signin" />;
}