import axios from "axios";

const url = process.env.REACT_APP_ENDPOINT;

export const getProductosByUser = async (username) => {
    try {
        const res = await axios.get(url + "/productos/user/"+username);
        return res;
    } catch (err) {
        return err.response.data;
    }
}
export const getProductos = async () => {
    try {
        const res = await axios.get(url + "/productos");
        return res;
    } catch (err) {
        return err.response.data;
    }
}

export const getProducto = async (id) => {
    try {
        const res = await axios.get(url + "/productos/" + id);
        return res.data;
    } catch (err) {
        return err.response.data;
    }
}

export const createProducto = async (data) => {
    try {
        const res = await axios.post(url + "/productos", data);
        return res;
    } catch (err) {
        return err.response.data;
    }
}
export const updateProducto = async (id, data) => {
    try {
        const res = await axios.put(url + "/productos/" + id, data);
        return res;
    } catch (err) {
        return err.response.data;
    }
}
export const deleteProducto = async (id) => {
    try {
        const res = await axios.delete(url + "/productos/" + id);
        return res;
    } catch (err) {
        return err.response.data;
    }
}
