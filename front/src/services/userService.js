import axios from "axios";

const url = process.env.REACT_APP_ENDPOINT;

export const loginUser = async (data) => {
  try {
    const res = await axios.post(url + "/auth/login", data);
    if (res.data != null) {
      //SPlit by : to get the token user:token
      const user = res.data.split(":")[0];
      const token = res.data.split(":")[1];
      sessionStorage.setItem("user", user);
      sessionStorage.setItem("token", token);
      
      return res;
    }
  } catch (err) {
    return err.response.data;
  }
};
export const logoutUser = async () => {
  try {
    sessionStorage.removeItem("user");
    sessionStorage.removeItem("token");
  } catch (err) {
    return err.response.data;
  }
}
export const getUser = async () => {
  try {
    const user = sessionStorage.getItem("user");
  
    const res = await axios.get(url + "/users/" + user);
    return res.data;
  } catch (err) {
    return err.response.data;
  }
}
