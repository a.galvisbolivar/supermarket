import axios from "axios";

const url = process.env.REACT_APP_ENDPOINT;

export const getVentas = async () => {
  try {
    const res = await axios.get(url + "/ventas");
    return res;
  } catch (err) {
    return err.response.data;
  }
}
export const getVenta = async (id) => {
    try {
        const res = await axios.get(url + "/ventas/" + id);
        return res.data;
    } catch (err) {
        return err.response.data;
    }
    }
export const createVenta = async (data) => {
    try {
        const res = await axios.post(url + "/ventas", data);
        return res;
    } catch (err) {
        return err.response.data;
    }
}
export const updateVenta = async (id, data) => {
    try {
        const res = await axios.put(url + "/ventas/" + id, data);
        return res;
    } catch (err) {
        return err.response.data;
    }
}
export const deleteVenta = async (id) => {
    try {
        const res = await axios.delete(url + "/ventas/" + id);
        return res;
    } catch (err) {
        return err.response.data;
    }
}
