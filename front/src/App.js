import { PrivateRoute } from "./components/PrivateRoute";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import Signin from "./views/Onboarding/SignIn";
import Home from "./views/Seller/Home";
import AppBar from "./views/AppBar/AppBar";
import NewProduct from "./views/Producto/NewProduct";
import Venta from "./views/Seller/Producto/Venta";
import Report from "./views/Seller/Report";

import "./App.css";

function App() {
  return (
    <div className="App">
      <Router>
        <AppBar />
        <Routes>
          <Route path="/" element={<PrivateRoute />}>
            <Route path="/" element={<Home />} />
          </Route>
          <Route path="/signin" element={<Signin />} />
          <Route path="/" element={<PrivateRoute />}>
            <Route path="/productos/nuevo" element={<NewProduct />} />
          </Route>
          <Route path="/producto/:id/venta" element={<PrivateRoute />}>
            <Route path="/producto/:id/venta" element={<Venta />} />
          </Route>
          <Route path="/productos/editar/:id" element={<PrivateRoute />}>
            <Route path="/productos/editar/:id" element={<NewProduct />} />
          </Route>
          <Route path="/reporte" element={<PrivateRoute />}>
            <Route path="/reporte" element={<Report />} />
          </Route>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
