import React, { useEffect, useState } from "react";
import Table from "../../components/Table/Table.js";
import { Grid, Container, Typography } from "@mui/material";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import DeleteForeverOutlinedIcon from "@mui/icons-material/DeleteForeverOutlined";
import SellIcon from '@mui/icons-material/Sell';
import { Link as RouterLink } from "react-router-dom";
import { getProductosByUser, deleteProducto } from "../../services/productService";
import { useUser } from "../../hooks/useAuthentication.js";

export default function Home() {
  const [productos, setProductos] = useState([]);
  const user = useUser();

  const namesHeader = [
    "id",
    "Nombre",
    "Cantidad",
    "ValorUnitario",
    "FechaCreación",
    "FechaActualización",
    "UsuarioCreación",
    "UsuarioActualización",
    "Acciones",
    "Vender"
  ];

  const borrar = async (id) => {
    //preguntar si esta seguro de borrar
    const sure = window.confirm("¿Esta seguro de borrar?");
    if (sure) {
      try {
        deleteProducto(id);
        setProductos(productos.filter((producto) => producto.id !== id));
      }
      catch (err) {
        console.log(err);
      }
    }
  }

  //get the data of the productos
  const acciones = (id) => {
    return (
      <div>
        <RouterLink to={`/productos/editar/${id}`}>
          <EditOutlinedIcon style={{ marginRight: "20%" }} />
        </RouterLink>
        
        <div onClick={() =>{borrar(id)}} className="pointer">
          <DeleteForeverOutlinedIcon />
        </div>
      </div>
    );
  };
  const venta = (id) => {
    return (
      <RouterLink to={`/producto/${id}/venta`}>
        <SellIcon  />
      </RouterLink>
    );
  }

  useEffect(() => {
    getProductosByUser(user)
      .then((res) => {
        res.data.map( producto => {
          producto.idUsuario = producto.user.username;
          delete producto.user;
          producto.acciones = acciones(producto.idProducto);
          producto.vender = venta(producto.idProducto)
          return producto;
      });
        setProductos(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <Container maxWidth="xl" style={{ padding: "5%", paddingTop: "12%" }}>
      <Grid container>
        <Grid item xs={8}>
          <Typography variant="h4" align="left" gutterBottom>
            Inventario de productos
          </Typography>
        </Grid>
        <Grid item xs={3}>
          <Typography variant="h6" align="center" gutterBottom>
            Añadir producto
            <RouterLink to="/productos/nuevo">
              <AddCircleOutlineIcon style={{ paddingTop: "2%" }} />
            </RouterLink>
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Table header={namesHeader} rows={productos} />
        </Grid>
      </Grid>
    </Container>
  );
}
