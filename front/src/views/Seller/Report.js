import React, { useEffect, useState } from "react";
import Table from "../../components/Table/Table.js";
import { Grid, Container, Typography } from "@mui/material";
import { getVentas } from "../../services/ventaService";

export default function Reporte() {
  const [ventas, setVentas] = useState([]);

  const namesHeader = [
    "id",
    "Cantidad Vendida",
    "Fecha Venta",
    "Nombre Producto",
    "Valor Unitario",
    "Cantidad Disponible Producto",
    "Total Venta"
  ];
  useEffect(() => {
    getVentas()
      .then((res) => {
        res.data.forEach((venta) => {
          venta.nombreProducto = venta.idProducto1.nombre;
          venta.valorUnitario = venta.idProducto1.valorUnitario;
          venta.disponible=venta.idProducto1.cantidad;
          venta.total=venta.valorUnitario*venta.cantidad;
          delete venta.idProducto;
          delete venta.idProducto1;
        });
        setVentas(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);


  return (
    <Container maxWidth="xl" style={{ padding: "5%", paddingTop: "12%" }}>
      <Grid container>
        <Grid item xs={8}>
          <Typography variant="h4" align="left" gutterBottom>
            Reporte de Ventas
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Table header={namesHeader} rows={ventas} />
        </Grid>
      </Grid>{" "}
    </Container>
  );
}
