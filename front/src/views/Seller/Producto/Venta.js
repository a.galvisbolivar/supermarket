import React, { useEffect, useState } from "react";
import { Grid, Paper, Typography, Button, TextField } from "@mui/material";
import { DateTimePicker } from "@mui/x-date-pickers/DateTimePicker";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { useNavigate, useParams } from "react-router-dom";
import { createVenta } from "../../../services/ventaService";
import { getProducto } from "../../../services/productService";

export default function Venta() {
  let { id } = useParams();
  const navigate = useNavigate();

  
  const [nombre, setNombre] = useState();
  const [cantidadProducto, setCantidadProducto] = useState();
  const [cantidadVenta, setCantidadVenta] = useState();
  const [valorUnitario, setValorUnitario] = useState();
  const [value, setValue] = React.useState(new Date("2022-07-19T21:11:54"));

  const handleChange = (newValue) => {
    setValue(newValue);
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    const producto = await getProducto(id);
    if (producto.cantidad < cantidadVenta) {
      alert("La cantidad de venta es mayor a la cantidad del producto");
    } else {
      const venta = {
        cantidad: cantidadVenta,
        fecha: value,
        idProducto: producto.idProducto,
      };
      createVenta(venta)
        .then(() => {
          navigate("/");
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  useEffect(() => {
    getProducto(id).then((producto) => {
      
      setNombre("Nombre:" + producto.nombre);
      setCantidadProducto("Cantidad disponible: " + producto.cantidad);
      setValorUnitario("Precio Unitario: " + producto.valorUnitario);
    });
  }, [id]);

  return (
    <div className="center">
      <Grid
        container
        alignItems="center"
        justifyContent="space-around"
        style={{ height: "500px", marginTop: "5%" }}
      >
        <Grid item xs={5}>
          <Paper elevation={3}>
            <br />
            <Grid
              item
              xs={12}
              align="center"
              style={{ margin: 10, marginRight: 10 }}
            >
              <Grid item xs={12}>
                <Typography component="h3" variant="h5">
                  Información del producto
                </Typography>
              </Grid>
              <Grid item xs={12} style={{ margin: 10 }}>
                <TextField
                  id="nombre"
                  name="Nombre"
                  disabled={true}
                  style={{ borderColor: "yellow" }}
                  fullWidth
                  value={nombre}
                  onChange={(e) => setNombre(e.target.value)}
                />
              </Grid>
              <Grid item xs={12} style={{ margin: 10 }}>
                <TextField
                  id="cantidad"
                  name="cantidad"
                  fullWidth
                  multiline
                  value={cantidadProducto}
                  disabled={true}
                />
              </Grid>
              <Grid item xs={12} style={{ margin: 10 }}>
                <TextField
                  disabled={true}
                  id="valorUnitario"
                  name="valorUnitario"
                  fullWidth
                  multiline
                  type="number"
                  value={valorUnitario}
                />
              </Grid>
              <Typography component="h3" variant="h5">
                Venta
              </Typography>

              <Grid item xs={12} style={{ margin: 10 }}>
                <TextField
                  id="cantidad venta"
                  name="cantidad venta"
                  label="Cantidad a vender"
                  fullWidth
                  multiline
                  type="number"
                  value={cantidadVenta}
                  onChange={(e) => setCantidadVenta(e.target.value)}
                />
              </Grid>

              <Grid item xs={12} style={{ margin: 10 }}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                  <DateTimePicker
                    label="Date&Time picker"
                    value={value}
                    onChange={handleChange}
                    renderInput={(params) => <TextField {...params} />}
                    fullWidth
                  />
                </LocalizationProvider>
              </Grid>
              <Grid item xs={12} style={{ margin: 10 }}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={(e) => handleSubmit(e)}
                  style={{ marginBottom: 30, marginTop: 10 }}
                >
                  Guardar
                </Button>
              </Grid>
              <br />
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}
