import React, { useState, useEffect } from "react";
import { Grid, Paper, Typography, Button, TextField } from "@mui/material";
import { useNavigate, useParams } from "react-router-dom";
import {
  createProducto,
  getProducto,
  updateProducto,
} from "../../services/productService";
import { getUser } from "../../services/userService";

export default function NewProduct() {
  let { id } = useParams();
  const [producto, setProducto] = useState();


  

  const navigate = useNavigate();
  const [nombre, setNombre] = useState(producto?.nombre ? producto.nombre : "");
  const [cantidad, setCantidad] = useState(
    producto?.cantidad ? producto.cantidad : ""
  );
  const [valorUnitario, setValorUnitario] = useState(
    producto?.valorUnitario ? producto.valorUnitario : ""
  );

  const handleSubmit = async (e) => {
    e.preventDefault();
    //preguntar si esta seguro
    const sure = window.confirm("¿Esta seguro de cargar el producto?");

    if (sure) {
      const user = await getUser();
      const producto = {
        nombre: nombre,
        cantidad: cantidad,
        valorUnitario: valorUnitario,
        idUsuario: user.idUser,
        idUsuarioActualizacion: user.idUser,
      };
      if (id) {
        updateProducto(id, producto)
          .then(() => {
            navigate("/");
          })
          .catch((err) => {
            console.log(err);
          });
      } else {
        createProducto(producto)
          .then(() => {
            navigate("/");
          })
          .catch((err) => {
            console.log(err);
          });
      }
    }
  };
  useEffect(() => {
    if (id) {
      getProducto(id)
        .then((res) => {
          setProducto(res);
          setNombre(res.nombre);
          setCantidad(res.cantidad);
          setValorUnitario(res.valorUnitario);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }
  , [id]);
  return (
    <div className="center">
      <Grid
        container
        alignItems="center"
        justifyContent="space-around"
        style={{ height: "500px", marginTop: "5%" }}
      >
        <Grid item xs={5}>
          <Paper elevation={3}>
            <br />
            <Grid
              item
              xs={12}
              align="center"
              style={{ margin: 10, marginRight: 10 }}
            >
              <Grid item xs={12}>
                <Typography component="h1" variant="h5">
                  Producto
                </Typography>
              </Grid>
              <Grid item xs={12} style={{ margin: 10 }}>
                <TextField
                  required
                  id="nombre"
                  name="Nombre"
                  label="Nombre del producto"
                  style={{ borderColor: "yellow" }}
                  fullWidth
                  value={nombre}
                  onChange={(e) => setNombre(e.target.value)}
                />
              </Grid>
              <Grid item xs={12} style={{ margin: 10 }}>
                <TextField
                  required
                  id="cantidad"
                  name="cantidad"
                  label="Cantidad del producto"
                  fullWidth
                  multiline
                  value={cantidad}
                  onChange={(e) => setCantidad(e.target.value)}
                />
              </Grid>
              <Grid item xs={12} style={{ margin: 10 }}>
                <TextField
                  required
                  id="valorUnitario"
                  name="valorUnitario"
                  label="Valor Unitario"
                  fullWidth
                  multiline
                  type="number"
                  value={valorUnitario}
                  onChange={(e) => setValorUnitario(e.target.value)}
                />
              </Grid>
              <Grid item xs={12} style={{ margin: 10 }}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={(e) => handleSubmit(e)}
                  style={{ marginBottom: 30, marginTop: 10 }}
                >
                  Guardar
                </Button>
              </Grid>
              <br />
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}
