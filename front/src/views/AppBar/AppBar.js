import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import Container from "@mui/material/Container";

import Button from "@mui/material/Button";
import { Link as RouterLink } from "react-router-dom";
import MenuItem from "@mui/material/MenuItem";
import { useAuthentication } from "../../hooks";
import { logoutUser } from "../../services/userService";

const pages = ["Inicio", "Reporte"];
const rutas = ["/", "/reporte", "/contactanos"];

const ResponsiveAppBar = () => {
  const auth = useAuthentication();

  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [texto, setTexto] = React.useState(
    auth ? "Cerrar Sesión" : "Iniciar Sesión"
  );

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  return (
    <AppBar position="fixed">
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "left",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "left",
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: "block", md: "none" },
              }}
            >
              {pages.map((page, index) => (
                <MenuItem
                  key={page}
                  onClick={handleCloseNavMenu}
                  component={RouterLink}
                  to={rutas[index]}
                >
                  <Typography textAlign="center">{page}</Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>

          <Box
            sx={{
              flexGrow: 1,
              display: { xs: "none", md: "flex" },
            }}
          >
            {pages.map((page, index) => (
              <div key={page} style={{ paddingLeft: "10%" }}>
                <Button
                  onClick={handleCloseNavMenu}
                  sx={{ my: 2, color: "white", display: "block" }}
                  component={RouterLink}
                  to={rutas[index]}
                >
                  {page}
                </Button>
              </div>
            ))}
          </Box>

          <Box sx={{ paddingRight: "10%" }}>
            <Button
              sx={{
                flexGrow: 1,
                color: "white",
                background: "rgb(12,12,157,0.6)",
                borderRadius: "50px",
                paddingLeft: "50px",
                paddingRight: "50px",
              }}
              onClick={() => {
                if (auth) {
                  logoutUser();
                  setTexto("Iniciar Sesión");
                }
              }}
              component={RouterLink}
              to="/signin"
            >
              {texto}
            </Button>
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
};
export default ResponsiveAppBar;
